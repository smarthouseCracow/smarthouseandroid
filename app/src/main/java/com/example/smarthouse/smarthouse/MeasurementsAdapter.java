package com.example.smarthouse.smarthouse;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by celina on 16.02.15.
 */
public class MeasurementsAdapter extends BaseAdapter {
    Typeface fontSensorName;
    Typeface fontTempAndOther;
    private ArrayList<MeasurementClass> listAdapter;
    private LayoutInflater mInflater;

    public MeasurementsAdapter(Context context, ArrayList<MeasurementClass> list) {

        listAdapter = list;
        mInflater = LayoutInflater.from(context);
        fontSensorName = Typeface.createFromAsset(context.getAssets(), "Sansation-Bold.ttf");
        fontTempAndOther = Typeface.createFromAsset(context.getAssets(), "Sansation-Light.ttf");

    }

    @Override
    public int getCount() {
        return listAdapter.size();
    }

    @Override
    public Object getItem(int position) {
        return listAdapter.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.items_list_view, null);
            holder = new ViewHolder();
            holder.txt_temp = (TextView) convertView.findViewById(R.id.list_temp);
            holder.txt_hum = (TextView) convertView.findViewById(R.id.list_hum);
            holder.txt_time = (TextView) convertView.findViewById(R.id.list_time);
            holder.txt_sensor = (TextView) convertView.findViewById(R.id.list_sensor);
            holder.txt_room = (TextView) convertView.findViewById(R.id.list_room);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.txt_room.setTypeface(fontSensorName);
        holder.txt_sensor.setTypeface(fontSensorName);
        holder.txt_temp.setTypeface(fontTempAndOther);
        holder.txt_hum.setTypeface(fontTempAndOther);
        holder.txt_time.setTypeface(fontTempAndOther);
        holder.txt_room.setText(listAdapter.get(position).nameRoom + " ");
        holder.txt_sensor.setText(listAdapter.get(position).nameSensor + " ");
        holder.txt_temp.setText(listAdapter.get(position).temperature + " \u00B0" + "C");
        holder.txt_hum.setText(listAdapter.get(position).humidity + " %RH");
        holder.txt_time.setText(listAdapter.get(position).timestamp + " ");


        return convertView;
    }


    static class ViewHolder {
        TextView txt_temp;
        TextView txt_hum;
        TextView txt_time;
        TextView txt_sensor;
        TextView txt_room;

    }

}
