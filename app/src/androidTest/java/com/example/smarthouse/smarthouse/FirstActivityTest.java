package com.example.smarthouse.smarthouse;

import android.app.Instrumentation;
import android.content.Intent;
import android.test.ActivityInstrumentationTestCase2;
import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import junit.framework.TestCase;

//public class FirstActivityTest extends InstrumentationTestCase {
//
//    public void test() throws Exception {
//        final int expected = 1;
//        final int reality = 1;
//        assertEquals(expected, reality);
//    }
//}

public class FirstActivityTest extends ActivityInstrumentationTestCase2<FirstActivity> {

    FirstActivity activity;
    Intent loginIntent;

    public FirstActivityTest() {

        super(FirstActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        activity= getActivity();
        loginIntent = new Intent(Intent.ACTION_MAIN);
    }
    @SmallTest
    public void testLayout()throws Exception{

        assertNotNull(activity.findViewById(R.id.login));
        assertNotNull(activity.findViewById(R.id.username));
        assertNotNull(activity.findViewById(R.id.password));
        assertNotNull(activity.remember);
        int buttonId = R.id.login;
        assertNotNull(buttonId);
    }




}
